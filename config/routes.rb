Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  #resources :devise
  #resources :test

  root 'accounts#index'

  resources :accounts 

  # Scope Users, Agreements, Campaigns and Subscriptions for each Tenant
  scope ':tenant' do
    
    #Resources for User from Devise
    devise_for :users, controllers: { registrations: "registrations", sessions: "sessions" }
    resources :devise

    #Resources for Campaign Subscription
    resources :campaigns do
      resources :subscriptions, only: [:new, :create]
    end

    #Resources for Agreements and Agreements Versions
    resources :agreements do
      collection do 
        get :deleted
      end
   
      resources :versions, only: [:destroy] do
        member do
          get :diff, to: 'versions#diff'
        end
      end
    end

    #Resources for Subscriptions
    #resources :subscriptions, only: [:index, :show]
    get 'subscriptions' => 'subscriptions#index', as: 'subscriptions'
    get 'subscriptions/:id' => 'subscriptions#show', as:'subscription'
    get 'subscriptions/:id/campaign' => 'subscriptions#campaign', as: 'campaign_subscription'
    get 'subscriptions/:id/agreement' => 'subscriptions#agreement', as: 'agreement_subscription'
    get 'subscriptions/:id/edit' => 'subscriptions#edit', as: 'edit_subscription'
    patch 'subscriptions/:id' => 'subscriptions#update'

  end

=begin
    resources :campaigns do
      resources :subscriptions, only: [:new, :create]
    end

    #resources :subscriptions, only: [:index, :show]
    get 'subscriptions' => 'subscriptions#index', as: 'subscriptions'
    get 'subscriptions/:id' => 'subscriptions#show', as:'subscription'
    get 'subscriptions/:id/campaign' => 'subscriptions#campaign', as: 'campaign_subscription'
    get 'subscriptions/:id/agreement' => 'subscriptions#agreement', as: 'agreement_subscription'
    get 'subscriptions/:id/edit' => 'subscriptions#edit', as: 'edit_subscription'
    patch 'subscriptions/:id' => 'subscriptions#update'
    
    #get 'campaigns/subscribe' => 'campaigns#subscribe'

    #mount Ckeditor::Engine => "/ckeditor"

    #get '/admin_users/login' => 'admin_users#login'
    #resources :admin_users

    root 'accounts#index'
    #root 'home#index'
    #root 'campaigns#index'

    resources :agreements do
      collection do 
        get :deleted
      end
   
      resources :versions, only: [:destroy] do
        member do
          get :diff, to: 'versions#diff'
        end
      end
    end
=end     

    #Resources for Versions to Bringback Agreements
    resources :versions, only: [] do
      member do
        patch :bringback
      end
    end

    get '/agreements/history', to: 'agreements#history', as: :agreement_history
    post '/agreements/:id/undo', to: 'agreements#undo', as: :undo

  #root 'campaign#show'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
