# Preview all emails at http://localhost:3000/rails/mailers/zara_user_mailer
class ZaraUserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/zara_user_mailer/signup_confirmation
  def signup_confirmation
    ZaraUserMailer.signup_confirmation
  end

end
