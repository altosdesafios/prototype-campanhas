class AddCampaignToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :campaign_id, :integer
  end
end
