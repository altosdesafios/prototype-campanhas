class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :name
      t.string :url
      t.string :contact_label
      t.string :phone_number
      t.string :cell_phone_number
      t.string :email

      t.timestamps null: false
    end
  end
end
