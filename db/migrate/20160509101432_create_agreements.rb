class CreateAgreements < ActiveRecord::Migration
  def change
    create_table :agreements do |t|
      t.string   :name
      t.text     :text
      t.datetime :publish_at
      t.datetime :discontinued_at
      t.integer  :admin_user_id
      t.string   :customer_id

      t.timestamps null: true
    end
  end
end
