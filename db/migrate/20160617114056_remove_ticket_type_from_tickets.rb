class RemoveTicketTypeFromTickets < ActiveRecord::Migration
  def change
     remove_column :tickets, :ticket_type
  end
end
