class AddIpToAgreementVersions < ActiveRecord::Migration
  def change
    add_column :agreement_versions, :ip, :string
  end
end
