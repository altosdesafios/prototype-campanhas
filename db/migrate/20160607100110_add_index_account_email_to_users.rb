class AddIndexAccountEmailToUsers < ActiveRecord::Migration
  def change
    add_index :users, [:account_id, :email], :unique => true
  end
end
