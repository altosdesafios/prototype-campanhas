class AddAccountToAgreements < ActiveRecord::Migration
  def change
    add_column :agreements, :account_id, :string
    add_index :agreements, :account_id
  end
end
