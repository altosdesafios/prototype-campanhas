class CreateTicketStatuses < ActiveRecord::Migration
  def change
    create_table :ticket_statuses do |t|
      t.string :name
      t.boolean :closed

      t.timestamps null: false
    end
  end
end
