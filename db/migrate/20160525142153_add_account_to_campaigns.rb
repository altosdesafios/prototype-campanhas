class AddAccountToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :account_id, :string
    add_index :campaigns, :account_id
  end
end
