class CreateTests < ActiveRecord::Migration
  def change
    create_table :tests do |t|
      t.string :first_name
      t.string :last_name
      t.date   :born_at
      t.string :email
      t.string :password
      t.string :password_confirmation

      t.timestamps null: false
    end
  end
end
