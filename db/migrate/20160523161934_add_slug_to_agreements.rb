class AddSlugToAgreements < ActiveRecord::Migration
  def change
    add_column :agreements, :slug, :string
    add_index :agreements, :slug, unique: true
  end
end
