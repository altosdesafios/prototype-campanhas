class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string  :email
      t.string  :address1
      t.string  :address2
      t.string  :zip_code
      t.string  :zip_location
      t.string  :phone_number
      t.string  :cell_phone_number
      t.string  :agreement_id
      t.string  :campaign_id
      t.string  :user_id

      t.timestamps null: false
    end
  end
end
