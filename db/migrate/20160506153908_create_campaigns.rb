class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string  :name
      t.boolean :active
      t.text    :introduction
      t.text    :description
      t.string  :product
      t.string  :image
      t.string  :agreement_id
      t.date    :start_date
      t.date    :end_date
      t.string  :customer_id

      t.timestamps null: false
    end
  end
end
