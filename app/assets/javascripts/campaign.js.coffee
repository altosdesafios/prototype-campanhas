jQuery ->
      $('#campaign_agreement_id').parent().hide()
      agreements = $('#campaign_agreement_id').html()
      $('#campaign_account_id').change ->
        account = $('#campaign_account_id :selected').text()
        escaped_account = account.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
        options = $(agreements).filter("optgroup[label='#{escaped_account}']").html()
        if options
          $('#campaign_agreement_id').html(options)
          $('#campaign_agreement_id').parent().show()
        else
          $('#campaign_agreement_id').empty()
          $('#cmapaign_lesson_id').empty()