class AgreementVersion < PaperTrail::Version

  self.table_name = :agreement_versions
  default_scope { where.not(event: 'create') } 
  
end