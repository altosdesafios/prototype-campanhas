class Subscription < ActiveRecord::Base

  belongs_to :campaign
  belongs_to :agreement
  belongs_to :user

  def self.subcribed_campaign(user,campaign)
    campaign.subscriptions.find_by( user_id: user.id)
  end

end
