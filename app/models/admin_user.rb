class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable

  has_many :agreements
  accepts_nested_attributes_for :agreements, :allow_destroy => true

   def self.find_version_author(version)
    find(version.terminator)   
  end

end
