class Agreement < ActiveRecord::Base

#  default_scope { where(customer_id: Customer.current_id) if Customer.current_id }


  acts_as_tenant(:account)
  validates_uniqueness_to_tenant :name

  
  belongs_to :admin_user
  belongs_to :account

  has_many :campaigns

  accepts_nested_attributes_for :campaigns, :allow_destroy => true

  def admin_user_name
    admin_user ? admin_user.id : ''
  end

  has_paper_trail
  has_paper_trail class_name: 'AgreementVersion'#, meta: { author_username: :user_name, word_count: :count_word}
  
  #has_paper_trail ignore: [:view_count]

  #has_paper_trail meta: { timestamp: Time.now }

end
