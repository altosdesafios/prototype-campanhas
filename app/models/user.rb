class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable #, :validatable

  #belongs_to :campaign_subscribe
  #has_paper_trail

  belongs_to :account

  #default_scope { where(account_id: Account.current_id) }

  acts_as_tenant(:account)
  validates_uniqueness_to_tenant :email, scope: :account_id

  #has_and_belongs_to_many :campaigns

  has_many :subscriptions
  has_many :campaigns, through: :subscriptions
  has_many :tickets

  accepts_nested_attributes_for :tickets, :allow_destroy => true  
  accepts_nested_attributes_for :campaigns, :allow_destroy => true
  accepts_nested_attributes_for :subscriptions, :allow_destroy => true

  #def self.find_version_author(version)
  #  find(version.terminator)   
  #end

  #validates_presence_of :first_name
  #validates_presence_of :last_name
  #validates_presence_of :born_at
  #validates_presence_of :email
  #validates_presence_of :password
  #validates_presence_of :password_confirmation

  def has_subscribed_campaign?(campaign)
    Subscription.subcribed_campaign(self,campaign).present?
  end
end
