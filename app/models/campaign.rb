class Campaign < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  
  scope :active, -> { where("campaigns.active = ?", true) }

#  default_scope { where(customer_id: Customer.current_id) if Customer.current_id }

  #def to_param
  #  "#{name}".parameterize
  #end

  #belongs_to :campaign_subscribe
  #has_paper_trail


  acts_as_tenant(:account)
  validates_uniqueness_to_tenant :name

  has_many :subscriptions
  #has_and_belongs_to_many :users, through: :subscriptions, class: :subscription
  has_many :users, through: :subscriptions
  accepts_nested_attributes_for :users, :allow_destroy => true

  has_many :tickets
  accepts_nested_attributes_for :tickets, :allow_destroy => true  

  belongs_to :agreement
  belongs_to :account

  accepts_nested_attributes_for :subscriptions, :allow_destroy => true
  
  mount_uploader :image, ImageUploader

end
