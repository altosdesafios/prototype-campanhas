class CampaignsUser < ActiveRecord::Base
  
  has_many :campaigns
  has_many :users
  has_many :agreements

  accepts_nested_attributes_for :campaigns, :allow_destroy => true
  accepts_nested_attributes_for :users, :allow_destroy => true
  accepts_nested_attributes_for :agreements, :allow_destroy => true
  
end
