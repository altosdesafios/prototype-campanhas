class Ticket < ActiveRecord::Base

  belongs_to :campaign
  belongs_to :priority
  belongs_to :ticket_status
  belongs_to :ticket_type
  belongs_to :user

end
