class Account < ActiveRecord::Base
  
  #extend FriendlyId
  #friendly_id :name, use: :slugged
  
  has_many :agreements
  has_many :campaigns
  has_many :users

  accepts_nested_attributes_for :agreements, :allow_destroy => true
  accepts_nested_attributes_for :campaigns, :allow_destroy => true
  accepts_nested_attributes_for :users, :allow_destroy => true

  def self.current_id=(id)
    Thread.current[:account_id] = id
  end

  def self.account_id
    Thread.current[:account_id]
  end
  
  mount_uploader :image, ImageUploader

end
