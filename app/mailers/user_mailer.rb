class UserMailer < ApplicationMailer
  default from: "registos@incentivehouse.pt"


  def signup_confirmation(user)
    @user = user

    path = user.account.image.url(:thumb)#.file.path
    attachments.inline['logo.png'] = File.read("#{Rails.root}/public" + path) #todo
    #attachments.inline['logo.png'] = File.read(Rails.root.join('public', path))
    #attachments.inline['logo.png'] = File.read("#{Rails.root}/app/assets/images/logo.png")
    mail to: user.email, subject: "Confirmação de Registo"
  end
end
