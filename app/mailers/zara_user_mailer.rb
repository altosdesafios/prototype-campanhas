class ZaraUserMailer < ApplicationMailer
  default from: "ccentofantead@gmail.com"


  def signup_confirmation(user)
    @user = user

    mail to: user.email, subject: "Zara Sign Up Confirmation"
  end
end
