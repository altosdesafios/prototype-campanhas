class CampaignsController < ApplicationController
  helper_method :campaigns_helper

  layout 'custom'

  #def to_param
  #  "#{id} #{name}".parameterize
  #end

  def index
    @campaigns = Campaign.active
    #@campaigns = current_customer.url

    if user_signed_in?
      @campaign = current_user.subscriptions
    end
  end
  
  def show
    @campaign = Campaign.friendly.find(params[:id])
    #@campaign = Campaign.active.find(params[:id])
  end

end