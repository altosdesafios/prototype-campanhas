class UsersController_lixo < InheritedResources::Base
  layout 'custom'
  private

    def user_params
      params.require(:user).permit(:first_name, :last_name, :born_at, :email, :password, :password_confirmation)
    end
end

