class SubscriptionsController < ApplicationController
  helper_method :campaigns_helper

  layout 'custom'
  before_action :current_user

  def agreement
    @subscription = Subscription.find(params[:id])
    @agreement = @subscription.agreement.version_at(@subscription.created_at)
    #byebug
  end
 
=begin 
  def campaign
    @subscription = Subscription.find(params[:id])
    @campaign = @subscription.campaign
  end
=end

  def index
    @subscriptions = current_user.subscriptions
  end
  
  def show
    @subscription = Subscription.find(params[:id])
    #@subscription = Campaign.find(params[:id])
  end

  def new
    @campaign = Campaign.friendly.find(params[:campaign_id])
    unless user_signed_in?
      session["user_return_to"] = new_campaign_subscription_path(@campaign, tenant: @campaign.account.url)
      redirect_to new_user_session_path and return
    end 

    @subscription = Subscription.new
    @subscription.campaign = @campaign
  end

  def edit
    @subscription = Subscription.find(params[:id])
  end

  def update
    @subscription = Subscription.find(params[:id])
    if @subscription.update(subscription_params)
      redirect_to subscription_path(@subscription)
    else
      render :edit
    end
  end

  def create
    @campaign = Campaign.friendly.find(params[:campaign_id])
    unless user_signed_in?
      session["user_return_to"] = new_campaign_subscription_path(@campaign, tenant: @campaign.account.url)
      redirect_to new_user_session_path and return
    end 

    #redirect_to campaigns_path
    @subscription = Subscription.new(subscription_params)
    @subscription.user_id = current_user.id
    @subscription.agreement_id = @campaign.agreement_id
    
    if @subscription.valid? && @subscription.save
      redirect_to subscription_path(@subscription, tenant: @campaign.account.url), notice: t(:subscription_ok)
    else
      render :new
    end
  end

  private

    def subscription_params
      params.require(:subscription).permit(:email, :address1, :address2, :zip_code, :zip_location, :phone_number, :cell_phone_number, :agreement_id, :campaign_id, :user_id)
    end

end