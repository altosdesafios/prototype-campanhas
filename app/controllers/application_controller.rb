class ApplicationController < ActionController::Base
  layout 'custom'
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :user_signed_in?

  before_filter :set_paper_trail_whodunnit
  before_filter :configure_permitted_parameters, if: :devise_controller?

  prepend_before_filter  :set_current_tenant_account
  set_current_tenant_through_filter

  attr_accessor :current_account

  def set_current_tenant_account
    if params[:tenant].present?
      @current_account = Account.find_by(url: params[:tenant])
      set_current_tenant(@current_account)
    end
  end

  helper_method :current_account
=begin
  def current_account
    @current_account
  end
  def current_account=(value)
    @current_account = value
  end
=end
#------------------------------------------------------------------------------------
=begin
  helper_method :current_account
  def current_account
    current_user.account
  end

  helper_method :scope_current_tenant
  def scope_current_account
    Account.current_id = current_account
    #Account.current_id = current_user.account.id if signed_in?
    yield
  ensure
    Account.current_id = nil
  end

  prepend_before_filter :set_tenant

  def set_tenant
    @account = Account.find_by(url: params[:url])
    set_current_tenant(@account)
  end
=end
#------------------------------------------------------------------------------------

  def index 
    @user = User.find(params[:id])
  end

  def require_user
    redirect_to new_user_session_path and return unless user_signed_in?
  end


  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :first_name
    devise_parameter_sanitizer.for(:sign_up) << :last_name
    devise_parameter_sanitizer.for(:sign_up) << :born_at

    devise_parameter_sanitizer.for(:account_update) << :first_name
    devise_parameter_sanitizer.for(:account_update) << :last_name
    devise_parameter_sanitizer.for(:account_update) << :born_at
  end  


  def info_for_paper_trail
    # Save additional info
    { ip: request.remote_ip }
  end


end
