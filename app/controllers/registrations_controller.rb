class RegistrationsController < Devise::RegistrationsController
  layout 'custom'
  protected

  def update_resource(resource, params)
    resource.update_without_password(params.except(:current_password))
  end

  def after_update_path_for(resource)
    campaigns_path
  end

  def after_sign_up_path_for(resource)
    campaigns_path
  end

  private

    def sign_up_params
      params.require(:user).permit(:first_name, :last_name, :born_at, :email, :password, :password_confirmation)
    end

    def account_update_params
      params.require(:user).permit(:first_name, :last_name, :born_at, :email, :password, :password_confirmation, :current_password)
    end

end

