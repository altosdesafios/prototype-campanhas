class VersionsController < ApplicationController
  #before_action :require_user
  before_action :set_agreement_and_version, only: [:diff]
 
  def diff
  end

  def bringback
    version = AgreementVersion.find(params[:id])
    @agreement = version.reify
    @agreement.save
    # Let's remove the version since the document is undeleted
    version.delete
    redirect_to agreement_path, notice: 'The document was successfully brought back!'
  end
 
  private
 
    def set_agreement_and_version
      @agreement = Agreement.find(params[:agreement_id])
      @version = @agreement.versions.find(params[:id])
    end
 
end