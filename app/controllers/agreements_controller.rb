class AgreementsController < ApplicationController
  before_action :current_user
  before_action :set_document, only: [:show, :edit, :update, :destroy]


  # GET /documents
  # GET /documents.json
  def index
    @agreements = Agreement.all
  end

  # GET /documents/1
  # GET /documents/1.json
  def show
    @agreement = Agreement.find(params[:id])
  end

  def deleted
    @agreements = AgreementVersion.where(event: 'destroy')
  end

  # GET /documents/new
  def new
    @agreements = Agreement.new
  end

  # GET /documents/1/edit
  def edit
    @agreement = Agreement.find(params[:id])
  end

  # POST /documents
  # POST /documents.json
  def create
    @agreements = Agreement.new(agreement_params)
    @agreements.admin_user_id = current_admin_user.id

    if @agreements.save
      redirect_to agreements_path(@agreement), notice: "New agreements saved."
    else
      render :new
    end
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    @agreements = Agreement.find(params[:id])
    if @agreements.update(agreement_params)
      #flash[:success] = "Agreement was updated! #{make_undo_link}"
      redirect_to agreements_path(@agreements)
    else
      render :edit
    end
  end
  

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @agreements = Agreement.find(params[:id])
    @agreements.destroy
    redirect_to agreements_path(@agreements), notice: 'Agreement was successfully destroyed.'
    
  end


  def history
    @versions = PaperTrail::Version.order('created_at DESC')
  end

  private

  #def undo
  #  @agreements_version = PaperTrail::Version.find_by_id(params[:id])

  #  begin
  #    if @agreements_version.reify
  #      @agreements_version.reify.save
  #    else
  #      # For undoing the create action
  #      @agreements_version.item.destroy
  #    end
  #    flash[:success] = "Undid that! #{make_redo_link}"
  #  rescue
  #    flash[:alert] = "Failed undoing the action..."
  #  ensure
  #    redirect_to root_path
  #  end
  #end

  #def make_undo_link
  #  view_context.link_to 'Undo that plz!', undo_path(@agreements.versions.last), method: :post
  #end

  #def make_redo_link
  #  params[:redo] == "true" ? link = "Undo that plz!" : link = "Redo that plz!"
  #  view_context.link_to link, undo_path(@agreement_version.next, redo: !params[:redo]), method: :post
  #end


  #Use callbacks to share common setup or constraints between actions.
  def set_document
    @agreements = Agreement.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def agreement_params
    params.require(:agreement).permit(:name, :text, :publish_at, :discontinued_at, :user_id)
  end
end
