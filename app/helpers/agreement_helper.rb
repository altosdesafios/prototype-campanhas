module AgreementHelper
  
  def find_version_author_name(version)
    admin_user = AdminUser.find_version_author(version) 
    admin_user ? admin_user.id : ''
  end

  def diff(text1, text2)
   changes = Diffy::Diff.new(text1, text2, include_plus_and_minus_in_html: true, include_diff_info: true)
   changes.to_s.present? ? changes.to_s(:html).html_safe : 'No Changes'
  end

end
