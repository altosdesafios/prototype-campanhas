module CampaignsHelper
  def show_subscribe_or_message(campaign, account)
    
    #subscription = current_user.has_subscribed_campaign?(campaign)
    subscription = current_user.subscriptions.find_by( campaign_id: campaign.id) if user_signed_in?

    if subscription
      subscription.created_at.strftime("%d/%m/%Y %H:%M")
      #t(:campaign_subscription)
    else
      link_to t(:subscribe), new_campaign_subscription_path(campaign, tenant: campaign.account.url), class: "btn btn-primary"
    end
  end
end
