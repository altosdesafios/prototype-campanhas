ActiveAdmin.register Subscription do

  permit_params :email, :address1, :address2, :zip_code, :zip_location, :phone_number, :cell_phone_number, :term, :campaign_id, :user_id

  index do
    selectable_column
    id_column
    column :email
    column :address1
    column :address2
    column :zip_code
    column :zip_location
    column :phone_number
    column :cell_phone_number
    column :agreement_id
    column :campaign_id, 'name'
    column :user_id, 'first_name'

    actions
  end

  filter :user_id

  form do |f|
    f.inputs "Subcriptions Details" do
      f.input :email
      f.input :address1
      f.input :address2
      f.input :zip_code
      f.input :zip_location
      f.input :phone_number
      f.input :cell_phone_number
      f.input :agreement_id
      f.input :campaign_id, as: :select, :collection => Campaign.all
      f.input :user_id, as: :select, :collection => User.pluck(:first_name, :id)
    end
    f.actions
  end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
