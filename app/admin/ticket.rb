ActiveAdmin.register Ticket do
  permit_params :title, :priority_id, :ticket_type_id, :user_id, :campaign_id, :ticket_status_id, :description

  index do
    selectable_column
    id_column
    column :title
    column :priority_id
    column :ticket_type_id
    column :user_id, 'first_name'
    column :campaign_id
    column :ticket_status_id
    column :description
    actions
  end

  form do |f|
    f.inputs "Tickets Details" do
      f.input :title
      f.input :priority_id, as: :select, :collection => Priority.all
      f.input :ticket_type_id, as: :select, :collection => TicketType.all
      f.input :user_id, as: :select, :collection => User.pluck(:first_name, :id)
      f.input :campaign_id, as: :select, :collection => Campaign.all
      f.input :ticket_status_id, as: :select, :collection => TicketStatus.all
      f.input :description
    end
    f.actions
  end
  
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
