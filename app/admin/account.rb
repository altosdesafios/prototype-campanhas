ActiveAdmin.register Account do
  permit_params :name, :url, :image, :contact_label, :phone_number, :cell_phone_number, :email

  index do
    selectable_column
    id_column
    column :name
    column :url
    column :image
    column :contact_label
    column :phone_number
    column :cell_phone_number
    column :email
    actions
  end

  filter :name
  filter :url
  
  form(:html => { :multipart => true }) do |f|
    f.inputs "Account Details" do
      f.input :name
      f.input :url
      f.input :image, :as => :file
      f.input :contact_label
      f.input :phone_number
      f.input :cell_phone_number
      f.input :email
    end
    f.actions
  end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
