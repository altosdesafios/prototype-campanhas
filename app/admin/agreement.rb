ActiveAdmin.register Agreement do
  permit_params :name, :account_id, :text, :publish_at, :discontinued_at, :admin_user_id

  index do
    selectable_column
    id_column
    column :name
    column :account_id
    #column :text
    column :admin_user_id#, 'first_name'
    actions
  end

  filter :name

  form do |f|
    f.inputs "Agreement Details" do
      f.input :name, label: "Version"
      f.input :account_id, as: :select, :collection => Account.all
      f.input :text, :as => :ckeditor
      f.input :admin_user_id, as: :select, :collection => AdminUser.all
    end
    f.actions
  end
  
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
