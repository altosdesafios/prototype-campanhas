ActiveAdmin.register User do
  permit_params :account_id, :first_name, :last_name, :born_at, :email, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column :account_id
    column :first_name
    column :last_name
    column :born_at
    column :email
    #column :current_sign_in_at
    #column :sign_in_count
    #column :created_at
    actions
  end

  filter :first_name
  filter :last_name
  filter :born_at
  filter :email

  form do |f|
    f.inputs "Users Details" do
      f.input :account_id, as: :select, :collection => Account.all
      f.input :first_name
      f.input :last_name
      f.input :born_at
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end
  
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
