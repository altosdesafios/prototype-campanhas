ActiveAdmin.register Campaign do
  permit_params :name, :account_id, :active, :introduction, :description, :product, :image, :agreement_id, :start_date, :end_date

  controller do
    def find_resource
      scoped_collection.friendly.find(params[:id])
    end
  end

  index do
    selectable_column
    id_column
    column :name
    column :account_id
    column :active
    column :introduction
    column :product
    column :image
    column :agreement_id#, 'name'
    column :start_date
    column :end_date
    actions
  end

  filter :name
  filter :start_date
  filter :end_date

  form(:html => { :multipart => true }) do |f|
    f.inputs "Campaign Details" do
      f.input :name
      f.input :active, :label => "Active Campaign"
      f.input :account_id, as: :select, :collection => Account.all
      #f.input :agreement_id, as: :select, :collection => Agreement.all
      f.input :agreement_id, as: :select, :collection => option_groups_from_collection_for_select(Account.all.order(:name), :agreements, :name, :id, :name)
      f.input :introduction
      f.input :description
      f.input :product
      f.input :image, :as => :file
      f.input :start_date
      f.input :end_date
    end
    f.actions
  end


# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
