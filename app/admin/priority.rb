ActiveAdmin.register Priority do
  permit_params :name, :default

  index do
    selectable_column
    id_column
    column :name
    column :default
    actions
  end

  form do |f|
    f.inputs "Priority Details" do
      f.input :name
      f.input :default
    end
    f.actions
  end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
