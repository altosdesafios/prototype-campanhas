json.array!(@agreements) do |agreement|
  json.extract! agreement, :id, :name, :text
  json.url agreement_url(agreement, format: :json)
end
