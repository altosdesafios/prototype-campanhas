json.array!(@tests) do |test|
  json.extract! test, :id, :first_name, :last_name, :born_at, :email, :password, :password_confirmation
  json.url test_url(test, format: :json)
end
